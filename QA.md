Create a new player
1. Open game with clean localStorage
2. Get prompt to create a user
3. On submit, Go to the menu

Failed Create a new player
1. Open game with clean localStorage
2. Get prompt to create a user, do not pass validation(too short
3. On submit, Show error to the user

Quick start
1. Open game
2. Go to click start
3. Game should start on all variations (difficulty/size)

Campaign levels win
1. Open game
2. Select a campaign level
3. On win, we should have three options (main menu, restart, next)
4. text should be green(or any color we choose to indicate success)

Campaign levels lose
1. Open game
2. Select a campaign level
3. On lose, we should have two options (main menu, restart)
4. text should be red(or any color we choose to indicate lose)

Levels - fighters shoud appear to be in delta formation
1. Open game
2. Select a campaign level
3. Attack a planet with more than 1 fighter, you should see a formation flying

Quick start
1. Open game
2. Select Quick start
3. you should see a correct title in the loading screen

Replaying previous maps
1. Open game
2. Be with a user that has 4 levels finished.
3. level 2 should be playable

Level help/hints
1. Open game
2. Select a campaign level
3. If the level has a welcome message, it should prompt to the user
4. If the level has a welcome message, clicking on the "?" button should show the welcome message as well

Analytics Events
- Start The app
- App done loading
- Level started
- Level won
- Level failed
- Player Created