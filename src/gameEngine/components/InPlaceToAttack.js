import {IN_PLACE_TO_ATTACK} from 'gameEngine/constants';
class InPlaceToAttack {
  constructor() {
    this.name = IN_PLACE_TO_ATTACK;
  }
}

export default InPlaceToAttack;