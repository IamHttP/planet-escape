import {MOVING} from 'gameEngine/constants';
class Moving {
  constructor(isMoving = true) {
    this.name = MOVING;
    this.isMoving = true;
  }
}

export default Moving;