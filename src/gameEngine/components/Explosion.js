import {EXPLOSION} from 'gameEngine/constants';
class Explosion {
  constructor() {
    this.name = EXPLOSION;
    this.times = 0;
  }
}

export default Explosion;